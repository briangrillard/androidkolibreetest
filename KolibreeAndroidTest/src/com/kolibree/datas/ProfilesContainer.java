package com.kolibree.datas;

import java.util.List;

public class ProfilesContainer {

	private String access_token = null;
	private boolean email_verified = false;
	private String facebook_id = null;
	private List<Profile> profiles;
	private String email = null;
	private int owner_profile_id;
	private int id;
	private String refresh_token = null;
	private String token_expires = null;

	public void addProfileToList(Profile object)
	{
		profiles.add(object);
	}
	
	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public boolean isEmail_verified() {
		return email_verified;
	}

	public void setEmail_verified(boolean email_verified) {
		this.email_verified = email_verified;
	}

	public String getFacebook_id() {
		return facebook_id;
	}

	public void setFacebook_id(String facebook_id) {
		this.facebook_id = facebook_id;
	}

	public List<Profile> getProfiles() {
		return profiles;
	}

	public void setProfiles(List<Profile> profiles) {
		this.profiles = profiles;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getOwner_profile_id() {
		return owner_profile_id;
	}

	public void setOwner_profile_id(int owner_profile_id) {
		this.owner_profile_id = owner_profile_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}

	public String getToken_expires() {
		return token_expires;
	}

	public void setToken_expires(String token_expires) {
		this.token_expires = token_expires;
	}

	@Override
	public String toString() {
		return "ProfilesContainer [access_token=" + access_token
				+ ", email_verified=" + email_verified + ", facebook_id="
				+ facebook_id + ", profiles=" + profiles + ", email=" + email
				+ ", owner_profile_id=" + owner_profile_id + ", id=" + id
				+ ", refresh_token=" + refresh_token + ", token_expires="
				+ token_expires + "]";
	}

}
