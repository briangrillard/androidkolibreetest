package com.kolibree.datas;

public class Profile {

	private String picture = null;
	private String first_name = null;
	private String last_name;
	private Stats stats = null;
	private boolean is_owner_profile = false;
	private String	address_country = null;
	private String	gender = null;
	private String address_state = null;
	private String address_city = null;
	private String survey_handedness = null;
	private String birthday = null;
	private String survey_toothbrush_type = null;
	private String address = null;
	private int account;
	private int brushing_goal_time;

	private int survey_brushing_time;
	private String address_zipcode = null;
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public Stats getStats() {
		return stats;
	}
	public void setStats(Stats stats) {
		this.stats = stats;
	}
	public boolean isIs_owner_profile() {
		return is_owner_profile;
	}
	public void setIs_owner_profile(boolean is_owner_profile) {
		this.is_owner_profile = is_owner_profile;
	}
	public String getAddress_country() {
		return address_country;
	}
	public void setAddress_country(String address_country) {
		this.address_country = address_country;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAddress_state() {
		return address_state;
	}
	public void setAddress_state(String address_state) {
		this.address_state = address_state;
	}
	public String getAddress_city() {
		return address_city;
	}
	public void setAddress_city(String address_city) {
		this.address_city = address_city;
	}
	public String getSurvey_handedness() {
		return survey_handedness;
	}
	public void setSurvey_handedness(String survey_handedness) {
		this.survey_handedness = survey_handedness;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getSurvey_toothbrush_type() {
		return survey_toothbrush_type;
	}
	public void setSurvey_toothbrush_type(String survey_toothbrush_type) {
		this.survey_toothbrush_type = survey_toothbrush_type;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getAccount() {
		return account;
	}
	public void setAccount(int account) {
		this.account = account;
	}
	public int getBrushing_goal_time() {
		return brushing_goal_time;
	}
	/*
	@Override
	public String toString() {
		return "Profile [picture=" + picture + ", first_name=" + first_name
				+ ", last_name=" + last_name + ", stats=" + stats
				+ ", is_owner_profile=" + is_owner_profile
				+ ", address_country=" + address_country + ", gender=" + gender
				+ ", address_state=" + address_state + ", address_city="
				+ address_city + ", survey_handedness=" + survey_handedness
				+ ", birthday=" + birthday + ", survey_toothbrush_type="
				+ survey_toothbrush_type + ", address=" + address
				+ ", account=" + account + ", brushing_goal_time="
				+ brushing_goal_time + ", survey_brushing_time="
				+ survey_brushing_time + ", address_zipcode=" + address_zipcode
				+ ", id=" + id + "]";
	}*/
	
	
	
	@Override
	public String toString()
	{
		return first_name;
	}
	
	public void setBrushing_goal_time(int brushing_goal_time) {
		this.brushing_goal_time = brushing_goal_time;
	}
	public int getSurvey_brushing_time() {
		return survey_brushing_time;
	}
	public void setSurvey_brushing_time(int survey_brushing_time) {
		this.survey_brushing_time = survey_brushing_time;
	}
	public String getAddress_zipcode() {
		return address_zipcode;
	}
	public void setAddress_zipcode(String address_zipcode) {
		this.address_zipcode = address_zipcode;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	int	id;
}
