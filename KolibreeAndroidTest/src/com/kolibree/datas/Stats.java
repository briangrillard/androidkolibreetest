package com.kolibree.datas;

public class Stats {

	private int profile;
	private int all_brushing_time;
	private int coins;
	private String last_brushing;
	private int	regular_brushing_day;
	private int total_visit;
	public int getProfile() {
		return profile;
	}
	public void setProfile(int profile) {
		this.profile = profile;
	}
	public int getAll_brushing_time() {
		return all_brushing_time;
	}
	public void setAll_brushing_time(int all_brushing_time) {
		this.all_brushing_time = all_brushing_time;
	}
	public int getCoins() {
		return coins;
	}
	public void setCoins(int coins) {
		this.coins = coins;
	}
	public String getLast_brushing() {
		return last_brushing;
	}
	public void setLast_brushing(String last_brushing) {
		this.last_brushing = last_brushing;
	}
	public int getRegular_brushing_day() {
		return regular_brushing_day;
	}
	public void setRegular_brushing_day(int regular_brushing_day) {
		this.regular_brushing_day = regular_brushing_day;
	}
	public int getTotal_visit() {
		return total_visit;
	}
	public void setTotal_visit(int total_visit) {
		this.total_visit = total_visit;
	}
	@Override
	public String toString() {
		return "Stats [profile=" + profile + ", all_brushing_time="
				+ all_brushing_time + ", coins=" + coins + ", last_brushing="
				+ last_brushing + ", regular_brushing_day="
				+ regular_brushing_day + ", total_visit=" + total_visit + "]";
	}

}
