package com.kolibree.datas;

public class NewProfile {

	private String first_name = null;
	private String last_name = null;
	private String gender;
	private String survey_handedness = null;
	private String birthday = null;
	

	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getSurvey_handedness() {
		return survey_handedness;
	}
	public void setSurvey_handedness(String survey_handedness) {
		this.survey_handedness = survey_handedness;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
}
