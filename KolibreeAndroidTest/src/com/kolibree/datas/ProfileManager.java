package com.kolibree.datas;

public class ProfileManager {

	static private ProfilesContainer	profilesContainer = null;
	
	static public void setProfilesContainer(ProfilesContainer pc)
	{
		profilesContainer = pc;
	}
	
	static public ProfilesContainer getProfilesContainer()
	{
		return profilesContainer;
	}
	
}
