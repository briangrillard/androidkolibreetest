package com.kolibree.sources;

import com.kolibree.datas.Profile;
import com.kolibree.datas.ProfileManager;
import com.kolibree.util.OnTaskCompletListerner;
import com.kolibree.util.TaskEnum;
import com.test.kolibree.R;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class AddProfile extends Activity {

	private int year;
	private int month;
	private int day;
	private Button dateButton;

	private String date = null;
	private String firstName = null;
	private String lastName = null;
	private String gender = null;
	private String hand = null;

	static final int DATE_DIALOG_ID = 999;
	private Context context = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_profile);
		context = this;

		addListener();
	}

	public void addListener() {
		addListenerOnDateButton();
		addCreateListener();
	}

	public void addCreateListener() {
		Button create = (Button) findViewById(R.id.createButton);

		create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				firstName = (((EditText) findViewById(R.id.firstName))
						.getText()).toString();
				lastName = (((EditText) findViewById(R.id.lastName)).getText())
						.toString();
				gender = (((Spinner) findViewById(R.id.gender))
						.getSelectedItem()).toString();
				hand = (((Spinner) findViewById(R.id.hand)).getSelectedItem())
						.toString();

				OnTaskCompletListerner listener = new OnTaskCompletListerner() {

					@Override
					public void onCompleteListerner(Object result) {
						// TODO Auto-generated method stub
						if (result == null)
							displayError();
						else {
							ProfileManager.getProfilesContainer()
									.addProfileToList((Profile) result);
							displaySuccess();
						}
					}

				};

				if (firstName == null || lastName == null || gender == null
						|| hand == null || date == null)
					displayError();
				else {
					NetWorkAsynTask nt = new NetWorkAsynTask(context, listener,
							TaskEnum.CREATEPROFILE, "/v1/accounts/"
									+ String.valueOf(ProfileManager
											.getProfilesContainer().getId())
									+ "/profiles/");
					if (hand.compareTo("Left") == 0)
						hand = "L";
					else
						hand = "R";
					if (gender.compareTo("Male") == 0)
						gender = "M";
					else
						gender = "F";
					nt.execute(firstName, lastName, gender, hand, date);
				}

			}

		});
	}

	public void displaySuccess() {
		TextView info = (TextView) findViewById(R.id.info);
		info.setVisibility(View.VISIBLE);
		info.setBackgroundResource(R.drawable.success_style);
		info.setText("The profile was created.");
	}

	public void displayError() {
		TextView info = (TextView) findViewById(R.id.info);
		info.setVisibility(View.VISIBLE);
		info.setBackgroundResource(R.drawable.error_style);
		info.setText("Please fill every field");
	}

	public void addListenerOnDateButton() {

		dateButton = (Button) findViewById(R.id.buttonDate);
		dateButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				System.out.println("button date clicked");
				onCreateDialog(DATE_DIALOG_ID).show();
			}
		});
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, datePickerListener, 1960, 01, 01);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;
			date = year + "-" + month + "-" + day;
			dateButton.setText(date);
		}
	};
}
