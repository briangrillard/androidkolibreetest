package com.kolibree.sources;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;

import com.kolibree.datas.NewProfile;
import com.kolibree.datas.ProfileManager;
import com.kolibree.json.JsonTreatment;

import android.util.Base64;


public class HttpRequests {

	private String secret = "X7doOhLCRbuT0FIgBsmy";
	private String absolute_url_reference = "https://test.api.kolibree.com";
	private String hash = null;
	private String urlRequest = null;
	private String clientId = "5";
	

	public HttpRequests(String url)
	{
		urlRequest = url;
	}
	
	public void sigGenerator() {
		try {
			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(),
					"HmacSHA256");
			sha256_HMAC.init(secret_key);
			hash = Base64.encodeToString(sha256_HMAC.doFinal((absolute_url_reference + urlRequest)
					.getBytes()), Base64.DEFAULT);
		} catch (Exception e) {
			System.out.println("Error");
		}
	}
	
	public String		connectToAccount(String email, String password) throws UnsupportedEncodingException
	{
		HashMap<String, String>	postDatas = new HashMap<String, String>();
		
		postDatas.put("email", email);
		postDatas.put("password", password);
		return executePostCall(absolute_url_reference + urlRequest, getPostDataString(postDatas));
	}
	
	
	public String		createProfile(String firstName, String lastName, String gender, String hand, String date) throws UnsupportedEncodingException
	{
		HashMap<String, String>	postDatas = new HashMap<String, String>();
		
		postDatas.put("first_name", firstName);
		postDatas.put("last_name", lastName);
		postDatas.put("gender", gender);
		postDatas.put("survey_handedness", hand);
		postDatas.put("birthday", date);
		NewProfile	profile = new NewProfile();
		profile.setBirthday(date);
		profile.setFirst_name(firstName);
		profile.setLast_name(lastName);
		profile.setGender(gender);
		profile.setSurvey_handedness(hand);
		String json = new JsonTreatment("").toJson(profile);
		System.out.println(json);
		System.out.println(ProfileManager.getProfilesContainer().getToken_expires());
		return executePostCall(absolute_url_reference + urlRequest, /*getPostDataString(postDatas)*/json);
	}
	
	
	public String  executePostCall(String requestURL, String postDatas) {

        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            
            connection.setReadTimeout(15000);
            connection.setConnectTimeout(15000);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches (false);
            connection.setRequestMethod("POST");

            
            connection.addRequestProperty("http-x-client-id", clientId);
            connection.addRequestProperty("http-x-client-sig", hash);
            if (postDatas.contains("gender"))
            {
                connection.addRequestProperty("http-x-access-token", ProfileManager.getProfilesContainer().getAccess_token());
                connection.addRequestProperty("Content-Type", "application/json");
            }
            else
            {
                connection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.addRequestProperty("Content-Length", "" + Integer.toString(postDatas.getBytes().length));
            }
            OutputStream os = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(postDatas);

            writer.flush();
            writer.close();
            os.close();
            
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            }
            else {
                response="";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
	
	private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException{
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return result.toString();
    }
	
	
	
	
	
	
}
