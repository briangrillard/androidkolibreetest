package com.kolibree.sources;

import java.io.UnsupportedEncodingException;

import com.kolibree.json.JsonTreatment;
import com.kolibree.util.OnTaskCompletListerner;
import com.kolibree.util.TaskEnum;
import com.test.kolibree.R;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

class NetWorkAsynTask extends AsyncTask<String, String, String> {

	TaskEnum	type;
	OnTaskCompletListerner	listener;
	String url = null;
	Context context = null;
	private ProgressDialog dialog;
	
	public NetWorkAsynTask(Context context, OnTaskCompletListerner listener, TaskEnum type, String url)
	{
		this.context = context;
		this.listener = listener;
		this.type = type;
		this.url = url;
		dialog = new ProgressDialog(context, R.style.MyTheme);
		dialog.setCancelable(false);
		dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
	}
	
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
               // do the works on url.....
    
    HttpRequests request = new HttpRequests(url);
	request.sigGenerator();
	String response = null;
    	try {
    		switch (type)
    		{
    		case GETACCOUNT:
    			response = request.connectToAccount(params[0], params[1]);
    			break;
    		case CREATEPROFILE:
    			response = request.createProfile(params[0], params[1], params[2], params[3], params[4]);
    			break; 
    		}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }

    @Override
    protected void onPostExecute(String result) {
    	if (dialog.isShowing()) {
            dialog.dismiss();
        }
    	System.out.println("Response: " + result);
    	JsonTreatment	conv = new JsonTreatment(result);
    	switch (type)
    	{
    		case GETACCOUNT:
    		System.out.println("CASE GET ACCOUNT IN POST");
    		listener.onCompleteListerner(conv.toObject());
    		break;
    		case CREATEPROFILE:
    			listener.onCompleteListerner(conv.toProfileObject());
    	}
    }
}