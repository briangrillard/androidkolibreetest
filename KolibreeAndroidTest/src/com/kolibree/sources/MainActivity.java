package com.kolibree.sources;

import com.kolibree.datas.ProfileManager;
import com.kolibree.datas.ProfilesContainer;
import com.kolibree.util.OnTaskCompletListerner;
import com.kolibree.util.TaskEnum;
import com.test.kolibree.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
	private OnTaskCompletListerner listener = null;
	private Context context = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		context = this;

		final TextView mSwitcher = (TextView) findViewById(R.id.welcome);
		mSwitcher.setText("Welcome to Kolibree !");

		final Animation in = new AlphaAnimation(0.0f, 1.0f);
		in.setDuration(3000);

		final Animation out = new AlphaAnimation(1.0f, 0.0f);
		out.setDuration(3000);

		mSwitcher.startAnimation(out);

		out.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationEnd(Animation animation) {
				mSwitcher.setText("Please log in !");
				mSwitcher.startAnimation(in);
				}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}
		});

		listener = new OnTaskCompletListerner() {

			@Override
			public void onCompleteListerner(Object result) {
				// TODO Auto-generated method stub
				if (result != null) {
					//System.out.println(((ProfilesContainer) result).toString());
					ProfileManager
							.setProfilesContainer((ProfilesContainer) result);
					Intent activity = new Intent(context,
							ListProfilesActivity.class);

					startActivity(activity);
				}
				else
				setError();

			}
		};
	}

	public void setError() {
		TextView layout = (TextView) findViewById(R.id.error);
		layout.setVisibility(View.VISIBLE);
	}

	public void login(View view) {
		String email = ((EditText) findViewById(R.id.email)).getText()
				.toString();
		String password = ((EditText) findViewById(R.id.password)).getText()
				.toString();
		
		// String email = "android.test@kolibree.com"; String password = "test";
		 
		if (email.matches(emailPattern) && (email.length() != 0)
				&& (password.length() != 0)) {
			NetWorkAsynTask nt = new NetWorkAsynTask(this, listener,
					TaskEnum.GETACCOUNT, "/v1/accounts/request_token/");
			nt.execute(email, password);
		} else
			setError();

	}
}
