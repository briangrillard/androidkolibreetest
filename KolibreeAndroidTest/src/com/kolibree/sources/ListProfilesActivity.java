package com.kolibree.sources;

import com.test.kolibree.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class ListProfilesActivity extends FragmentActivity implements
		HeadlinesFragment.OnHeadlineSelectedListener {

	private HeadlinesFragment firstFragment = null;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_profile);

		if (findViewById(R.id.fragment_container) != null) {
			if (savedInstanceState != null) {
				return;
			}

			firstFragment = new HeadlinesFragment();

			firstFragment.setArguments(getIntent().getExtras());

			getSupportFragmentManager().beginTransaction()
					.add(R.id.fragment_container, firstFragment).commit();

		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		System.out.println("On activity Result");
		if (firstFragment != null)
		firstFragment.initAdapter();
	}

	@Override
	public void onContentSelected(int position) {

		ProfileFragment articleFrag = (ProfileFragment) getSupportFragmentManager()
				.findFragmentById(R.id.content_fragment);

		if (articleFrag != null) {
			articleFrag.updateArticleView(position);

		} else {
			changeFragment(position);
		}
	}

	public void changeFragment(int position) {
		ProfileFragment newFragment = new ProfileFragment();
		Bundle args = new Bundle();
		args.putInt(ProfileFragment.ARG_POSITION, position);
		newFragment.setArguments(args);
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();

		transaction.replace(R.id.fragment_container, newFragment);
		transaction.addToBackStack(null);

		transaction.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.addProfile:
			Intent intent = new Intent(this, AddProfile.class);
			startActivityForResult(intent, 1);
			break;

		default:
			break;
		}

		return true;
	}

}
