package com.kolibree.sources;

import java.util.ArrayList;

import com.kolibree.datas.Profile;
import com.kolibree.datas.ProfileManager;
import com.test.kolibree.R;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.GridView;
import android.widget.TextView;

public class ProfileFragment extends Fragment {
	public final static String ARG_POSITION = "position";
	int mCurrentPosition = -1;
	private View rootView = null;
	public GridView gridView;
	public int position = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		if (savedInstanceState != null) {
			mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
		}

		rootView = inflater
				.inflate(R.layout.profile_fragment, container, false);
		updateArticleView(0);
		return rootView;
	}

	@Override
	public void onStart() {
		super.onStart();

		Bundle args = getArguments();
		if (args != null) {
			updateArticleView(args.getInt(ARG_POSITION));
		} else if (mCurrentPosition != -1) {
			updateArticleView(mCurrentPosition);
		}
	}

	public void updateArticleView(int position) {

		Log.d("updateArticleVoew", String.valueOf(mCurrentPosition));

		Profile profile = ProfileManager.getProfilesContainer().getProfiles()
				.get(position);

		ArrayList<Element> elem = new ArrayList<Element>();

		
		elem.add(new Element((TextView) rootView.findViewById(R.id.name),
				"", profile.getFirst_name() + " " + profile.getLast_name()));
		
		
		elem.add(new Element(((TextView) rootView.findViewById(R.id.coinText)),
				"Coins: ", String.valueOf(profile.getStats().getCoins())));
		// coins.setText("x" + profile.getStats().getCoins());

		elem.add(new Element(((TextView) rootView
				.findViewById(R.id.totalBrushing)), "Burshing Time: ", String
				.valueOf(profile.getStats().getAll_brushing_time())));
		elem.add(new Element((TextView) rootView.findViewById(R.id.goalTime),
				"Goal Time: ", String.valueOf(profile.getBrushing_goal_time())));

		elem.add(new Element((TextView) rootView
				.findViewById(R.id.lastBrushing), "Last Brushing: ", profile
				.getStats().getLast_brushing()));

		elem.add(new Element((TextView) rootView.findViewById(R.id.totalVisit),
				"Total Visit: ", String.valueOf(profile.getStats()
						.getTotal_visit())));

		elem.add(new Element((TextView) rootView.findViewById(R.id.hand),
				"Handedness: ", profile.getSurvey_handedness()));


		elem.add(new Element((TextView) rootView.findViewById(R.id.birthday),
				"Birthday: ", profile.getBirthday()));

		elem.add(new Element((TextView) rootView.findViewById(R.id.id), "Id: ",
				String.valueOf(profile.getId())));

		elem.add(new Element((TextView) rootView.findViewById(R.id.gender), "Gender: ",
				String.valueOf(profile.getGender())));
		
		elem.add(new Element((TextView) rootView.findViewById(R.id.toothbrushType), "ToothBrush type: ",
				String.valueOf(profile.getSurvey_toothbrush_type())));
		
		elem.add(new Element((TextView) rootView.findViewById(R.id.adress), "Adress: ",
				String.valueOf(profile.getAddress())));
	
		mCurrentPosition = position;
		animateText(elem);

	}

	public class Element {
		public TextView view = null;
		public String text;

		public Element(TextView view, String text, String value) {
			this.view = view;
			this.text = text;
			if (value == null || value.isEmpty()) {
				this.text += "Undefinded";
			} else
				this.text += value;
		}

		public void setText() {
			view.setText(text);

		}
	}

	public void animateText(final ArrayList<Element> elem) {

		final Animation in = new AlphaAnimation(0.0f, 1.0f);
		in.setDuration(150);

		elem.get(0).setText();
		elem.get(0).view.startAnimation(in);

		in.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationEnd(Animation animation) {
				
				if (position != elem.size())
				elem.get(position).view.clearAnimation();
				position++;
				if (position != elem.size()) {
					elem.get(position).setText();
					elem.get(position).view.startAnimation(in);
				}
				else
					position = 0;
			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}
		});

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// Save the current article selection in case we need to recreate the
		// fragment
		outState.putInt(ARG_POSITION, mCurrentPosition);
	}
}