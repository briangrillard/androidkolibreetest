package com.kolibree.json;

import com.google.gson.Gson;
import com.kolibree.datas.NewProfile;
import com.kolibree.datas.Profile;
import com.kolibree.datas.ProfilesContainer;

public class JsonTreatment {

	String	datas = null;
	Gson	gson = null;
	
	public JsonTreatment(String data)
	{
		datas = data;
		gson = new Gson();
	}
	
	public ProfilesContainer	toObject()
	{
		
		ProfilesContainer obj = gson.fromJson(datas, ProfilesContainer.class);
		return obj;
	}
	
	public Profile toProfileObject()
	{
		Profile obj = gson.fromJson(datas, Profile.class);
		return obj;
	}
	
	public String	toJson(NewProfile profile)
	{
		return gson.toJson(profile);
	}
}
